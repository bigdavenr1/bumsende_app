package de.bumsen.de_app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.InflateException;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 
 * @author Tino Stief
 * Startklasse der App erbt von Activity (Thread) und implementiert den Locationlistener
 * Prüfu ob Internetverbindung existiert und entscheidet anhand des Ergebnisses
 */
public class MainActivity extends Activity implements LocationListener
{
	// Variablen setzen
	SharedPreferences myPrefs;
 	boolean gpsOn=false, netOn=false, netchecked=false;
 	TextView statistictxt, statustxt;
 	ImageView waiting;
 	ImageButton suche_starten;
 	String infos;
 	public static JSONArray jsonArray;
 	
 	// neues System-Object zum Prüfen der Connectivität setzen
  	ConnectivityManager cm ;
  	// NetInfo Objekt (abgeleitet vom Connectivitätsmanager setzen (liest Status der Netzverbindung aus)
    NetworkInfo netInfo;
    // Neues Timerelement
    Timer autoUpdate;
 	
    /**
     * Standardfunktion on Create (Wird beim erstellen der View ausgeführt)
     */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		// Alle threads akzeptieren
    	StrictMode.ThreadPolicy policy = new StrictMode.
    	ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy);
		
    	// Superklasse importieren
        super.onCreate(savedInstanceState);
        
        // Ein View setzen (hier activity_main.xml
        setContentView(R.layout.activity_main);
        
        //Elemente zum Ansprechen einbinden 
	    statistictxt= (TextView)findViewById(R.id.Statistictext);
	    statustxt= (TextView)findViewById(R.id.Statustext);
		suche_starten = (ImageButton) findViewById(R.id.suche_starten);
		ImageView logo = (ImageView) findViewById(R.id.logo_final);
		cm= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    netInfo= cm.getActiveNetworkInfo();
	    waiting=(ImageView) findViewById(R.id.waiting);
	    Animation rotate = AnimationUtils.loadAnimation(this, R.drawable.loading);
		
		//logo positionieren
	    RelativeLayout.LayoutParams log = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
	    log.leftMargin=0;
	    log.rightMargin=0;
	    log.topMargin=0;
	    logo.setLayoutParams(log);     
	    
	    // Ladeanimation starten
	    waiting.startAnimation(rotate);
	  
	    // Statustext setzen und sichtbar machen
	    statustxt.append("Warte auf Internetverbindung...\nstellen Sie bitte sicher,\ndass eine Internetverbindung besteht!");
	    statustxt.setVisibility(2);
	    
	    // Timer (1000 Milisekunden) für die Prüfung der Verbindung setzen
	    autoUpdate = new Timer();
	    autoUpdate.schedule(new TimerTask() 
	    {
            @Override
            public void run() 
            {
                runOnUiThread(new Runnable() 
                {
                    public void run() 
                    {
                    	// Wenn Netz gefunden
                        if (checkNetStat()==true)
                        {
                        	// Informationen holen
                        	getInformations();
                        	// Timer beenden
                        	autoUpdate.cancel();
                        }
                        // Wenn kein Netz gefunden
                        else
                        {
                        	// Warnung anzeigen, falls noch nicht geschehen
                        	showNetwarn();
                        }
                    }
                });
            }
        }, 0, 1000);
	}
	
	/**
	 * Funktion zum Abholen von Informationen vom angeschlossenen Webserver
	 */
	public void getInformations()
	{
		// Timer wird beendet
		autoUpdate.cancel();
		// Warteanimation wird ausgeblendet
		waiting.setVisibility(0);
		// Startbutton wird eingeblendet
		suche_starten.setVisibility(2);
		// Statustext wird geändert
		statustxt.setText("Verbindung hergestellt...");
		// Onklicklistener auf den Startbutton setzen
		suche_starten.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
            	// Alle Bilder löschen mit GarbageCollector
      		    unbindDrawables(findViewById(R.id.activity_main));
	   	        System.gc();
            	// neue Activity starten (Browser)
            	startActivity(new Intent(MainActivity.this,StartSearch.class));
            	android.os.Process.killProcess(android.os.Process.myPid());
            }
        });
		// Versuch die DAten aus dem Netz zu holen 
		try  
		{
	    	// JSON-Array aus ausgelesenen Daten erstellen
			try 
			{
				jsonArray = new JSONArray(readInfos());
				// Wenn Array da
				if(jsonArray!=null)
				{				
					// Daten auslesen und anzeigen
					JSONObject test=jsonArray.getJSONObject(0);
				    statistictxt.setText("Statistic"+"\n"+"private Anbieter: "+test.getString("anbieteranzahl")+"\n"+"Clubs: "+test.getString("clubanzahl"));
				}
			} 
			catch (JSONException e) {statistictxt.setText("Fehler beim Einlesen der Statistikdaten");}			
		} 
		catch (InterruptedException e){	statistictxt.setText("Fehler beim Einlesen der Statistikdaten");}
	}
	
	/**
	 * Funktion zum Anzeigen des Warndialogs, 
	 */
	public void showNetwarn()
	{
		if(netchecked==false)
		{
			// Alert Dialog initialisieren
			AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
			// Messaage setzen
			builder.setMessage("Es muss eine aktive Internetverbindung vorhanden sein! Bitte aktivieren Sie diese Optionen!")
	        // kann nur über Button beendet und nicht abgebrochen werden
	        .setCancelable(false)
	        // Button setzen der zu Einstellungen führt
	        .setPositiveButton("Einstellungen", new DialogInterface.OnClickListener() 
	        {
	        	// onclicklistener auf Button der zu einstellungen führt
	            public void onClick(DialogInterface dialog, int id) 
	            {
	            	// neue Activität starten -> Einstellungsoptionen direkt von Android und auf Gpoogle-Standortdienste
	            	Intent gpsOptionsIntent = new Intent(
	            	android.provider.Settings.ACTION_SETTINGS);
	            	startActivity(gpsOptionsIntent);
	            }
	        })
	        // Button setzen der die EInstellungen übergeht (keine Lokalisierung möglich
	        .setNegativeButton("Nein", new DialogInterface.OnClickListener() 
	        {
	        	// Onclicklistener auf diesen Button
	            public void onClick(DialogInterface dialog, int id)
	            {
	            	// Dialog abbrechen
	                 dialog.cancel();
	            }
	        });
			// Dialog zusammenbasteln und anzeigen
			AlertDialog alert = builder.create();
			alert.show();
			netchecked=true;
		}
	}

	/**
	 * Standardfunktionen
	 * 
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	@Override
	public void onLocationChanged(Location location) {}
	@Override
	public void onProviderDisabled(String provider) {}
	@Override
	public void onProviderEnabled(String provider) {}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {}	
	// Wenn Activity beendet wird
	protected void onDestroy() 
	{
		 super.onDestroy();
		 // Alle Bilder über GarbageCollector löschen (Methode aufrufen)
		 unbindDrawables(findViewById(R.id.activity_main));
		 System.gc();
	}
	
    /**
     *  Methode zum löschen aller Grafiken aus dem Speicher
     * @param view für welches View
     */
	private void unbindDrawables(View view) 
	{
		// Wenn Hintergrund gesetzt, Hintergrund löschen
		if (view.getBackground() != null) 
		    view.getBackground().setCallback(null);
		// Wenn weitere Elemente vorhanden
		if (view instanceof ViewGroup) 
		{
			// Alle Instancen durchgehen und löschen
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) 
			    unbindDrawables(((ViewGroup) view).getChildAt(i));
		    ((ViewGroup) view).removeAllViews();
	    }
	}	
	
	/**
	 *  Funktion zum Auslesen der Statisticdaten
	 * @return
	 * @throws InterruptedException
	 */
	public String readInfos() throws InterruptedException 
	{
		// Stringbuilder instanziieren (abstracter Datentyp)
		StringBuilder builder = new StringBuilder();
		// HTTP-Client instanziieren (abstrakter DAtentyp
		HttpClient client = new DefaultHttpClient();
		// URL setzen (Zum Holen der Daten)
		HttpGet httpGet = new HttpGet("http://host-b.nb-cooperation.de/app/getInformations/getInformations.php");
		try 
		{
			// Anfrage ausführen
			HttpResponse response = client.execute(httpGet);
			// Statusline erhalten
			StatusLine statusLine = response.getStatusLine();
			// Statuscode erhalten
			int statusCode = statusLine.getStatusCode();
			// Wenn Status OK
			if (statusCode == 200) 
			{
				// Ergebnis der Abfrage in Variable speichern
				HttpEntity entity = response.getEntity();
				// Inhalt des HTTP-Response speichern
				InputStream content = entity.getContent();
				// Buffer instanziieren und Inhalt hineinspeichern
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				// Line erzeugen
				String line;
				// Buffer zeilenweise durchgehen
				while ((line = reader.readLine()) != null)
					// Line dem Stringbuilder hinzufügen
					builder.append(line);
			} 
			// Wenn status nicht ok
			else Log.e("Fehler", "Failed to download file");
		} 
		catch (ClientProtocolException e) {e.printStackTrace();} 
		catch (IOException e) {	e.printStackTrace();}
		catch (InflateException e) {	e.printStackTrace();}
		// String aus Builder zurückgeben
		return "["+builder.toString()+"]";
	}
	
	/**
	 * Methode zum Prüfen der Internetverbindung 
	 * @return boolean 
	 */
	public boolean checkNetStat()
	{
		// Netinfo neu holen
		cm= (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
 	    netInfo= cm.getActiveNetworkInfo();
 	    // Prüfen ob Netzwerkverbindung besteht
		if(netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED)// && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)==true)
	    	return true;
		else
			return false;
	}	
}