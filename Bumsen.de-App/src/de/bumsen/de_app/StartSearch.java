package de.bumsen.de_app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.InflateException;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import de.bumsen.de_app.RangeSeekBar.OnRangeSeekBarChangeListener;

public class StartSearch extends Activity
{
	// Initialisierung verschiedenster Variablen
	protected static final String TAG = null;
    Bundle korb = new Bundle();    
    String chooseOrt;
    CharSequence ort;
    RelativeLayout orteview;
    LinearLayout  orteviewlist;
    ScrollView ortescroll;
    EditText ortField; 
    SeekBar entfernung;
    ImageButton suche_starten;
    TextView minPreisField, maxPreisField, maxEntfernungsField, minGroesseField, maxGroesseField,minAlterField,maxAlterField,minKoerbchenField,maxKoerbchenField;
    public static JSONArray jsonArray;
    
	@Override
	// Methode wenn die Activity das erste mal gestartet wird
    public void onCreate(Bundle savedInstanceState) 
	{
		// TODO Es muss noch der Startort ausgelesen werden (entweder per Netzwerk oder per GPS)
		// Alle threads akzeptieren
    	StrictMode.ThreadPolicy policy = new StrictMode.
    	ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy);
		
    	// Superklasse importieren
        super.onCreate(savedInstanceState);
        
        // Ein View setzen (hier activity_main.xml
        setContentView(R.layout.searchform);
        // globalen Context setzen
        Context context = getApplicationContext();		
        
        // Alle Auswahlfelder registrieren für späteren Zugriff
        ortField=(EditText)findViewById(R.id.editText1);
        entfernung=(SeekBar)findViewById(R.id.seekBar1);
        minPreisField=(TextView)findViewById(R.id.minPreisField);
        maxPreisField=(TextView)findViewById(R.id.maxPreisField);
        maxEntfernungsField=(TextView)findViewById(R.id.maxEntfernungsField);
        minAlterField=(TextView)findViewById(R.id.minAlterField);
        maxAlterField=(TextView)findViewById(R.id.maxAlterField);
        minGroesseField=(TextView)findViewById(R.id.minGroesseField);
        maxGroesseField=(TextView)findViewById(R.id.maxGroesseField);
        minKoerbchenField=(TextView)findViewById(R.id.minKoerbchenField);
        maxKoerbchenField=(TextView)findViewById(R.id.maxKoerbchenField);
        orteview=(RelativeLayout)findViewById(R.id.orteview);
        orteviewlist = (LinearLayout)findViewById(R.id.orteviewlist);
        suche_starten = (ImageButton) findViewById(R.id.suche_starten);
        
        /**
         * Listener aufs Ändern des Textes
         */
        ortField.addTextChangedListener(new TextWatcher() 
        {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) 
            {
            	// Eingabe zwischen speichern
            	ort=s;
            	
            	if (chooseOrt!=null && s.toString().equals(chooseOrt))
        			orteview.setVisibility(View.INVISIBLE);
            	else if(ort.length()>0)
            	{            		
            		getOrteByInput(s);
            		orteview.setVisibility(View.VISIBLE);
            	}
        		else
        			orteview.setVisibility(View.INVISIBLE);
            }            
			
			@Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
			@Override
			public void afterTextChanged(Editable arg0) {}
        });
        
        /**
         * Listener aufs Ändern des Schiebereglers Entfernung
         */
        korb.putString("maxEntfernung", "0");
        entfernung.setOnSeekBarChangeListener(new OnSeekBarChangeListener()
        {    
            // Wenn Schieberegler geschoben wird
        	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
        	{
        		// maximale Entfernung speichern und die Anzeige aktualisieren
        		korb.putString("maxEntfernung", String.valueOf(progress));
        		maxEntfernungsField.setText(String.valueOf(progress)+" km");
        	}
			@Override
			public void onStartTrackingTouch(SeekBar arg0) {}
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        
        korb.putString("minPreis", "0");
		korb.putString("maxPreis", "1000");
        // Preis je Stunde
        RangeSeekBar<Integer> preisBar = new RangeSeekBar<Integer>(0, 1000, context );
        preisBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() 
        {
        	// Wenn Schieberegler geschoben wird
        	@Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) 
        	{
        		// minamel und MAximalwert speichern und die Anzeige aktualisieren
        		korb.putString("minPreis", String.valueOf(minValue));
        		korb.putString("maxPreis", String.valueOf(maxValue));
        		minPreisField.setText(String.valueOf(minValue)+" EUR");
        		maxPreisField.setText(String.valueOf(maxValue)+" EUR");
            }
        });
        
        ViewGroup preisBarLayout = (ViewGroup) findViewById(R.id.preisbalken);
        preisBarLayout.addView(preisBar);
        
        // Alter RangeSeekBar
        korb.putString("minAlter", "18");
		korb.putString("maxAlter", "99");
        RangeSeekBar<Integer> alterBar = new RangeSeekBar<Integer>(18, 99, context );
        alterBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() 
        {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) 
            {
            	korb.putString("minAlter", String.valueOf(minValue));
        		korb.putString("maxAlter", String.valueOf(maxValue));
        		minAlterField.setText(String.valueOf(minValue));
        		maxAlterField.setText(String.valueOf(maxValue));
            }
        });
        // add RangeSeekBar to pre-defined layout
        ViewGroup alterBarLayout = (ViewGroup) findViewById(R.id.alterbalken);
        alterBarLayout.addView(alterBar);
        
        // Körpergröße
        korb.putString("minGroesse", "0");
		korb.putString("maxGroesse", "250");
        RangeSeekBar<Integer> groesseBar = new RangeSeekBar<Integer>(0, 250, context );
        groesseBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() 
        {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) 
            {
            	korb.putString("minGroesse", String.valueOf(minValue));
        		korb.putString("maxGroesse", String.valueOf(maxValue));
        		minGroesseField.setText(String.valueOf(minValue)+" cm");
        		maxGroesseField.setText(String.valueOf(maxValue)+" cm");
            }
        });
        // add RangeSeekBar to pre-defined layout
        ViewGroup groesseBarLayout  = (ViewGroup) findViewById(R.id.groessebalken);
        groesseBarLayout.addView(groesseBar );
        
        korb.putString("minKoerbchen", "1");
		korb.putString("maxKoerbchen", "7");
        // Körbcghengröße
        RangeSeekBar<Integer> koerbchenBar  = new RangeSeekBar<Integer>(1, 7, context );
        koerbchenBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() 
        {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar, Integer minValue, Integer maxValue) 
            {
        		korb.putString("minKoerbchen", String.valueOf(minValue));
        		korb.putString("maxKoerbchen", String.valueOf(maxValue));
        		String koerbchenArray[]= {"A","B","C","D","E","F","G"};
        		minKoerbchenField.setText(koerbchenArray[(minValue-1)]);
        		maxKoerbchenField.setText(koerbchenArray[(maxValue-1)]);
            }
        });
        // add RangeSeekBar to pre-defined layout
        ViewGroup koerbchenBarLayout = (ViewGroup) findViewById(R.id.koerbchenbalken);
        koerbchenBarLayout.addView(koerbchenBar);        
        
        suche_starten.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
            	// neue Activität starten -> Einstellungsoptionen direkt von Android und auf Gpoogle-Standortdienste
            	Intent in = new Intent(StartSearch.this, DoSearch.class);
            	in.putExtras(korb);
            	startActivity(in);
            	// Alle Bilder löschen mit GarbageCollector
            	unbindDrawables(findViewById(R.id.searchform));
	   	        System.gc();
            	// neue Activity starten (Browser)
            	//startActivity(new Intent(StartSearch.this,DoSearch.class));
            	android.os.Process.killProcess(android.os.Process.myPid());
            }
        });       
	}
	 /** 
     * methode zum holen der Orte für dei Suche (AJAX)
     */
	private void getOrteByInput(CharSequence s) 
	{
		// Versuch die DAten aus dem Netz zu holen 
		try  
		{
	    	// JSON-Array aus ausgelesenen Daten erstellen
			try 
			{
				jsonArray = new JSONArray(readInfos(s));
				// Wenn Array da
				if(jsonArray!=null)
				{				
					// Daten auslesen und anzeigen
					JSONObject test=jsonArray.getJSONObject(0);
					
					RelativeLayout.LayoutParams orte = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
					orteview.setLayoutParams(orte);
					final int[] x=new int[2];
					ortField.getLocationOnScreen(x);
			        final int top=x[0];
			        final int height = ortField.getHeight();
			        
			        orte.topMargin=top+height;
			        orte.leftMargin=35;
			        orte.rightMargin=35;
			        orte.bottomMargin=10;
					orteview.setLayoutParams(orte);
					orteview.setPadding(4, 4, 4, 4);
					for (int i = 0; i < ((ViewGroup) orteviewlist).getChildCount(); i++) 					    
						((ViewGroup) orteviewlist).removeAllViews();
					
					JSONArray cities= new JSONArray(test.getString("data"));
					
					TextView[] tx = new TextView[100];
					// Alle Instancen durchgehen und löschen
					for (int i = 0; i < cities.length(); i++)
					{
						JSONArray cityname= new JSONArray(cities.getString(i));
						
						final String tt=cityname.getString(0);
						tx[i] = new TextView(this);
						tx[i].setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
					
						tx[i].setWidth(300);
						tx[i].setHeight(45);
						tx[i].setPadding(0, 10, 0, 0);
				        tx[i].setText(tt);
				        
				        orteviewlist.addView(tx[i]);
				        tx[i].setOnClickListener(new OnClickListener() 
				        {
				        	@Override 
				        	public void onClick(View v) 
				        	{
				        		chooseOrt=tt;
				        		korb.putString("Ort", tt);				        		
				        		setOrt(tt);			        		
				        	}
				        	
							private void setOrt(String tt) 
							{							
								try
								{
									orteview.setVisibility(View.INVISIBLE);	
									ortField.setText(tt);		
									for (int i = 0; i < ((ViewGroup) orteviewlist).getChildCount(); i++) 					    
										((ViewGroup) orteviewlist).removeAllViews();
								}
								catch (Exception e)	{e.printStackTrace();}
							}
						});
					}
					   
				}
			} 
			catch (JSONException e) {e.printStackTrace();}			
		} 
		catch (InterruptedException e) {e.printStackTrace();}
	}
	/**
	 *  Funktion zum Auslesen der Statisticdaten
	 * @return
	 * @throws InterruptedException
	 */
	public String readInfos(CharSequence s) throws InterruptedException 
	{
		// Stringbuilder instanziieren (abstracter Datentyp)
		StringBuilder builder = new StringBuilder();
		// HTTP-Client instanziieren (abstrakter DAtentyp
		HttpClient client = new DefaultHttpClient();
		// URL setzen (Zum Holen der Daten)
		String param=s.toString();
		//URL url = new URL("http://host-b.nb-cooperation.de/app/getcity/getcity.php?search="+param);
		HttpGet httpGet = new HttpGet("http://host-b.nb-cooperation.de/app/getcity/getcity.php?search="+Uri.encode(param));
		try 
		{
			// Anfrage ausführen
			HttpResponse response = client.execute(httpGet);
			// Statusline erhalten
			StatusLine statusLine = response.getStatusLine();
			// Statuscode erhalten
			int statusCode = statusLine.getStatusCode();
			// Wenn Status OK
			if (statusCode == 200) 
			{
				// Ergebnis der Abfrage in Variable speichern
				HttpEntity entity = response.getEntity();
				// Inhalt des HTTP-Response speichern
				InputStream content = entity.getContent();
				// Buffer instanziieren und Inhalt hineinspeichern
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				// Line erzeugen
				String line;
				// Buffer zeilenweise durchgehen
				while ((line = reader.readLine()) != null)
					// Line dem Stringbuilder hinzufügen
					builder.append(line);
			} 
			// Wenn status nicht ok
			else Log.e("Fehler", "Failed to download file");
		} 
		catch (ClientProtocolException e) {e.printStackTrace();} 
		catch (IOException e) {	e.printStackTrace();}
		catch (InflateException e) {	e.printStackTrace();}
		// String aus Builder zurückgeben
		return "["+builder.toString()+"]";
	}	
	 /**
     *  Methode zum löschen aller Grafiken aus dem Speicher
     * @param view für welches View
     */
	private void unbindDrawables(View view) 
	{
		// Wenn Hintergrund gesetzt, Hintergrund löschen
		if (view.getBackground() != null) 
		    view.getBackground().setCallback(null);
		// Wenn weitere Elemente vorhanden
		if (view instanceof ViewGroup) 
		{
			// Alle Instancen durchgehen und löschen
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) 
			    unbindDrawables(((ViewGroup) view).getChildAt(i));
		    ((ViewGroup) view).removeAllViews();
	    }
	}
}