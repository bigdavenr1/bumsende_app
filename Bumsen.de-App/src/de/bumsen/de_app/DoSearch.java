package de.bumsen.de_app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.InflateException;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class DoSearch extends Activity
{
	Bundle searchdata;
	public static JSONArray jsonArray;
	LinearLayout  searchMatchList;
	String param="";
	ScrollView searchscroll=new ScrollView(this);
	LinearLayout searchList=new LinearLayout(this);
	
	
	@Override
	// Methode wenn die Activity das erste mal gestartet wird
    public void onCreate(Bundle savedInstanceState) 
	{
		
		// Alle threads akzeptieren
    	StrictMode.ThreadPolicy policy = new StrictMode.
    	ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy);
		
    	// Superklasse importieren
        super.onCreate(savedInstanceState);
        
        // übergebene Suchdaten
        searchdata = getIntent().getExtras();
        param+="ort="+Uri.encode(searchdata.getString("Ort"));
        param+="&maxEntfernung="+searchdata.getString("maxEntfernung");
        param+="&minGroesse="+searchdata.getString("minGroesse");
        param+="&maxGroesse="+searchdata.getString("maxGroesse");
        param+="&minKoerbchen="+searchdata.getString("minKoerbchen");
        param+="&maxKoerbchen="+searchdata.getString("maxKoerbchen");
        param+="&minAlter="+searchdata.getString("minAlter");
        param+="&maxAlter="+searchdata.getString("maxAlter");
        param+="&minPreis="+searchdata.getString("minPreis");
        param+="&maxPreis="+searchdata.getString("maxPreis");
        
        RelativeLayout.LayoutParams orte = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        this.addContentView(searchscroll, orte);
        
        // Suche beim Start einmal ausführen
        getSearch(param);        
	}
	
	 /** 
     * methode zum Auswerten der Suchergebnisse (AJAX)
     */
	private void getSearch(String param) 
	{
		// Versuch die DAten aus dem Netz zu holen 
		try  
		{
	    	// JSON-Array aus ausgelesenen Daten erstellen
			try 
			{
				// Daten auslesen und als Object bereits stellen
				JSONObject data=new JSONArray(readInfos(param)).getJSONObject(0);
				// Wenn Array da
				if(	data!=null)
				{			
					
					// Anbieterdaten herausfiltern
					JSONObject anbieterdataobj=new JSONArray("["+data.getString("anbieter")+"]").getJSONObject(0);
					JSONArray anbieterdata=new JSONArray("["+anbieterdataobj.getString("data")+"]");
					Integer anbieteranzahl=anbieterdataobj.getInt("anzahl");
					Integer anbieterlimit=anbieterdataobj.getInt("limit");
				
				    // Clubdaten heruasfiltern
					JSONObject clubdataobj=new JSONArray("["+data.getString("clubs")+"]").getJSONObject(0);
					JSONArray clubdata=new JSONArray("["+anbieterdataobj.getString("data")+"]");
					Integer clubanzahl=anbieterdataobj.getInt("anzahl");
					Integer clublimit=anbieterdataobj.getInt("limit");
					
			        
					/*for (int i = 0; i < ((ViewGroup) searchMatchList).getChildCount(); i++) 					    
						((ViewGroup) searchMatchList).removeAllViews();*/
					
					
					//JSONArray daten= new JSONArray(test.getString("data"));
					//JSONArray anbieter= new JSONArray(test.getString("anbieter"));
					//JSONArray clubs= new JSONArray(test.getString("clubs"));
					
					
					
					/*TextView[] tx = new TextView[100];
					// Alle Instancen durchgehen und löschen
					for (int i = 0; i < anbieter.length(); i++)
					{
						JSONArray cityname= new JSONArray(anbieter.getString(i));
						
						final String tt=cityname.getString(0);
						tx[i] = new TextView(this);
						tx[i].setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
					
						tx[i].setWidth(300);
						tx[i].setHeight(45);
						tx[i].setPadding(0, 10, 0, 0);
				        tx[i].setText(tt);
				        final int ind=i;
				        
				        searchMatchList.addView(tx[i]);
				        tx[i].setOnClickListener(new OnClickListener() 
				        {
				        	@Override 
				        	public void onClick(View v) 
				        	{
				        		showProfile(ind);	
				        	}
				        
							
						});
					}*/
					   
				}
			} 
			catch (JSONException e) {e.printStackTrace();}			
		} 
		catch (InterruptedException e) {e.printStackTrace();}
	}
	
	
	/**
	 *  Funktion zum Auslesen der gesuchten Daten
	 * @return String als JSON-Object
	 * @throws InterruptedException
	 */
	public String readInfos(String param) throws InterruptedException 
	{
		// Stringbuilder instanziieren (abstracter Datentyp)
		StringBuilder builder = new StringBuilder();
		// HTTP-Client instanziieren (abstrakter DAtentyp
		HttpClient client = new DefaultHttpClient();
		// URL setzen (Zum Holen der Daten)
		HttpGet httpGet = new HttpGet("http://host-b.nb-cooperation.de/app/dosearch/dosearch.php?"+param);
		try 
		{
			// Anfrage ausführen
			HttpResponse response = client.execute(httpGet);
			// Statusline erhalten
			StatusLine statusLine = response.getStatusLine();
			// Statuscode erhalten
			int statusCode = statusLine.getStatusCode();
			// Wenn Status OK
			if (statusCode == 200) 
			{
				// Ergebnis der Abfrage in Variable speichern
				HttpEntity entity = response.getEntity();
				// Inhalt des HTTP-Response speichern
				InputStream content = entity.getContent();
				// Buffer instanziieren und Inhalt hineinspeichern
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				// Line erzeugen
				String line;
				// Buffer zeilenweise durchgehen
				while ((line = reader.readLine()) != null)
					// Line dem Stringbuilder hinzufügen
					builder.append(line);
			} 
			// Wenn status nicht ok
			else Log.e("Fehler", "Failed to download file"+httpGet.getURI());
		} 
		catch (ClientProtocolException e) {e.printStackTrace();} 
		catch (IOException e) {	e.printStackTrace();}
		catch (InflateException e) {e.printStackTrace();}
		// String aus Builder zurückgeben
		return "["+builder.toString()+"]";
	}	
	
	private void showProfile(int i) {
		// TODO Auto-generated method stub
		
	}

}
